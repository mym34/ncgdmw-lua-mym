# NCGDMW Lua Edition

A "Natural Grow" leveling mod where your attributes and level grow automatically as your skills increase, and your skills will also decay over time (Optional).

**OpenMW 0.48 or newer is required!**

A changelog is available in the [CHANGELOG.md](CHANGELOG.md) file, and an FAQ in the [FAQ.md](FAQ.md) file.

#### Credits

This is a port of [Greywander's "Natural Character Growth and Decay - MW" mod](https://www.nexusmods.com/morrowind/mods/44967) for the OpenMW-Lua API.

##### Original concept, MWScript edition author

Greywander

##### Lua edition authors

EvilEye, johnnyhostile, Mehdi Yousfi-Monod

MBSP Uncapper: [Phoenixwarrior7](https://www.nexusmods.com/morrowind/users/6708039)

NCGDMW Patches: Alvazir

##### Localization

PT_BR: Karolz

DE: Atahualpa

ES: drumvee

FR: [Rob from Rob's Red Hot Spot](https://www.youtube.com/channel/UCue7D0dm2SilBhbVe3SB75g)

PL: emcek

RU: [dmbaturin](https://baturin.org/)

#### Installation

This mod has several different installation options. If you aren't sure which to use:

* For a mostly vanilla setup balance-wise, go with `NCGDMW + New Bonuses & Multipliers`
* For a setup that's balanced to be more difficult, go with `NCGDMW + Double Bonuses & Multipliers`

##### OpenMW Dev Build Options

This setup includes the OpenMW Dev Build support option, intended for use with the latest dev builds.

```
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\00 Core"
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\00 OpenMW-DevBuild"
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\02 NewNCGD"

content=ncgdmw.omwaddon
content=ncgdmw.omwscripts
content=ncgdmw-dev.omwscripts
```

##### NCGDMW + Classic Bonuses & Multipliers

This setup will use the "classic" bonus and multiplier changes carried over from the old NCGD:

```
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\00 Core"
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\01 ClassicNCGD"

content=ncgdmw.omwaddon
content=ncgdmw.omwscripts
# Don't use this birthsigns patch with OpenMW 0.49!!
content=ncgdmw-vanilla-birthsigns-patch.omwaddon
```

##### NCGDMW + New Bonuses & Multipliers

The "NewNCGD" setup keeps vanilla Morrowind values for the various skill bonus and multiplier GMSTs:

```
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\00 Core"
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\02 NewNCGD"

content=ncgdmw.omwaddon
content=ncgdmw.omwscripts
# Don't use this birthsigns patch with OpenMW 0.49!!
content=ncgdmw-vanilla-birthsigns-patch.omwaddon
```

##### NCGDMW + Double Bonuses & Multipliers

The "NewNCGDDoubleSkillBonuses" setup doubles the vanilla Morrowind values for the various skill bonus and multiplier GMSTs:

```
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\00 Core"
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\03 NewNCGDDoubleSkillBonuses"

content=ncgdmw.omwaddon
content=ncgdmw.omwscripts
# Don't use this birthsigns patch with OpenMW 0.49!!
content=ncgdmw-vanilla-birthsigns-patch.omwaddon
```

##### NCGDMW Alt Start

The "AltStart" setup is intended for use with total conversion mods such as [Arktwend](https://www.moddb.com/mods/arktwend-the-forgotten-realm):

```
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\00 Core"
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\04 AltStart (For Total Conversions)"

content=ncgdmw_alt_start.omwaddon
content=ncgdmw.omwscripts
```

##### NCGDMW Starwind

The "Starwind" setup is intended for use with [Starwind](https://www.nexusmods.com/morrowind/mods/48909):

```
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\00 Core"
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\05 Starwind"

content=ncgdmw_starwind.omwaddon
content=ncgdmw.omwscripts
```

##### NCGDMW Patches

The `06 Patches` option comes with patches for several mods that replace "Ability" spell effects with "Curses" (which as indicated in the FAQ play more nicely with this mod).

Patches are provided for the following mods:

* [Abandoned Flat](https://modding-openmw.gitlab.io/abandoned-flat/)
* [Havish](https://www.moddb.com/downloads/havish-v13)
* [Meteors](https://modding-openmw.com/mods/meteors/)
* [OAAB_Data](https://www.nexusmods.com/morrowind/mods/49042)
* [Sotha Sil Expanded](https://www.nexusmods.com/morrowind/mods/42347)
* [Tamriel_Data](https://www.nexusmods.com/morrowind/mods/44537)
* [Tel Aruhn Chronicles](https://www.nexusmods.com/morrowind/mods/49171) (and [BCOM](https://www.nexusmods.com/morrowind/mods/49231))
* [Thief Experience Overhaul](https://modlist.altervista.org/mmh/?quick_filter=LDones)

```
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\00 Core"
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\03 NewNCGDDoubleSkillBonuses"
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\06 Patches"

content=ncgdmw.omwaddon
content=ncgdmw.omwscripts
...
content=Meteor - NCGDMW_Lua Patch.omwaddon
...
content=OAAB_Data - NCGDMW_Lua Patch.omwaddon
...
```

Many thanks to Alvazir for making these patches and letting me include them!

##### MBSP + Uncapper

All credit for [originally creating this code](https://www.nexusmods.com/morrowind/mods/53064) goes to [Phoenixwarrior7](https://www.nexusmods.com/morrowind/users/6708039).

Adjusts the skill increase from casting spells based on how much magicka they cost (versus the vanilla behavior of the same amount of growth for any spell).

```
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\00 Core"
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\07 MBSP_Uncapper"

content=ncgdmw.omwaddon
content=ncgdmw.omwscripts
content=ncgdmw-vanilla-birthsigns-patch.omwaddon
content=MBSP_Uncapper.omwscripts
```

Changes I've made to this since forking:

* **0.49 only**: Play a sound when skills level up
* **0.49 only**: Use the thick border in the menu
* **0.49 only**: Support the new `MENU` API for the hotkey setting

This is still fully compatible with OpenMW 0.48.

##### Vanilla Birthsigns Patch

Also included is the `ncgdmw-vanilla-birthsigns-patch.omwaddon` plugin which edits "The Lady", "The Lover", and "The Steed" birthsigns found in the vanilla game to be "Curses" instead of an "Ability", thus reflecting their fortifactions more accurately in the final stat value.

**Unfortunately** doing this breaks the birthsign UI since it is hardcoded to look for abilities. **This does not affect OpenMW 0.49**.

Any birthsign mod that provides a birthsign which fortifies an attribute or skill should provide a compatibility version that uses "Curses".

#### How It Works

##### Attributes

Attribute values are calculated based on the value of governed skills. There is a spreadsheet included (`00 Core/docs/ncgd skills.ods`) that breaks down the relations between attributes and skills as well as their weights.

##### Leveling

The player level is calculated based on the value of skills with various maths applied. It, like attributes, is calculated automatically as your skills increase and decrease.

##### Decay

The decay mechanic can be a nice way to balance the game and stave off becoming too powerful too quickly. Some things to keep in mind about this mechanic:

* Decay won't begin right away; depending on the speed you've selected it could take between three and seventeen days of game time to begin.
* A skill cannot decay past one half of it's previous maximum value. So if your Acrobatics skill was at 50, it can't decay past 25.
* Regardless of a skill's max value, it will not decay below 15.
* When you level up a skill, a small amount of decay progress is removed.

#### Optional Features

This mod comes with various optional features which are described below.

##### Disable Skill Gains From Books

Off by default, this feature lets you disable raising skill levels from reading a book.

##### Adjustable HP Growth

Your base HP and per level HP gain are based on a weighted average of endurance, strength and willpower: `(endurance * 4 + strength * 2 + willpower) / 7`. This option will let you decide how much HP you'll gain per level:

* High: 10% of the average value
* Low: 5%

##### Adjustable Skill Gain Rate

Two options are provided for adjusting skill gain rates, and **they may used together**:

* **Constant Ratio**: Reduce every skill gain for any action by some constant value.
* **Squared Level Ratio**: Skill gain reductions will evolve exponentially, having a very low impact at low level skills and a very high impact at high level skills.

##### State-Based HP

**This is considered an experimental feature!**

Adjusts HP values based on the state of your HP, also factoring fortifications and all other external changes.

##### Magicka-Based Skill Progression

* **Magicka Skill Experience** is the vanilla Skill Experience * (Spell Cost / Experience Rate (default is 10)). This means that if your spell costs 10 Magicka, you will be leveling at the default rate. If it costs 20, you will be leveling twice as fast, etc... This multiplier can be configured in the settings. Since, this is a multiple of the Vanilla skill experience, it automatically factors in Major/Minor/Misc skill bonuses/penalties as well as skill specialization bonuses.
* **Magicka Refund** uses the following formula: Spell Cost * SQRT(Skill - Refund Start) * Refund Mult / 100
* **Refund Start** is the skill level at which you start receiving Magicka refunds, this can be configured in the settings, default is 35.
* **Refund Mult** is a configurable value that allows you to tweak the Magicka refund to your liking. Ranges from 1-5, default is 4.

##### Skill Uncapper

**Skill Uncapper** applies to all skills, not just Magicka skills. While skills are below the vanilla limit of 100, you shouldn't notice any difference in behavior. You will be able to see skill progress in the default game menus like normal. However, once skills have gone above 99, you will need to use a custom menu hotkey (configurable) to view skill progress. This menu uses the same format as the NCGDMW Lua decay progress menu.

#### Compatibility With Other Mods

Some mods out in the wild may produce unexpected behavior when used with NCGDMW Lua Edition. Please see the "Mod Conflict" tag [on the project issue tracker](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/?label_name=Mod%20Conflict) for more information about known, unsolved cases of this.

#### Known Compatibility Issues

Aside from what's stated in the [`FAQ.md`](FAQ.md) document about compatibility, there are some known issues with unknown causes out there.

* I've gotten reports of unexpected behavior when using NCGDMW Lua Edition with [GV's OpenMW TR+OAAB+TOTSP+BCOM+Fixes](https://next.nexusmods.com/morrowind/collections/hvnlyl) mod list. I don't have the resources to install this list and debug myself (collections aren't usable on Linux, my OS) but if you're willing to help test this it'd be most welcome! Please see [this issue](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/30) for more information.

#### Help Localize NCGDMW

Do you speak a language that's not yet offered and want to contribute a new localization? Follow these steps:

1. Download a release zip from [this URL](https://modding-openmw.gitlab.io/ncgdmw-lua/)
1. Open the `00 Core/l10n/NCGDMW/en.yaml` file with your favorite text editor ([Notepad++](https://notepad-plus-plus.org/) is recommended for Windows)
1. Update each line (the quoted part after the `:`) as desired
1. Save the file with the name `<CODE>.yaml`, where `<CODE>` is the [language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) for the localization you're adding
1. Commit your change to git and [open a merge request](https://gitlab.com/modding-openmw/ncgdmw-lua/-/merge_requests/new), or simply email the file to `admin@modding-openmw.com` with `New NCGD Localization` as the subject

#### Web

* [Project Home](https://modding-openmw.gitlab.io/ncgdmw-lua/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53136)
* [Source on GitLab](https://gitlab.com/modding-openmw/ncgdmw-lua)

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/new) for bug reports or feature requests
* Email: `johnnyhostile at modding-openmw dot com`
* Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/53136?tab=posts)

#### Planned Features

Not yet implemented (not yet possible):

* Optional level up song and animation
* View decay progress by mousing over a skill
* [Request a feature!](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/new)
